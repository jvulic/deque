spell:
	@ if ! which misspell > /dev/null; then \
			echo "error: misspell not installed" >&2; \
			exit 1; \
		fi
	@ misspell -w .

lint:
	@ if ! which gometalinter > /dev/null; then \
			echo "error: gometalinter not installed" >&2; \
			exit 1; \
		fi
	@ gometalinter --exclude=proto --disable-all --enable=golint --enable=vet --enable=gofmt ./... 

test:
	go test .

bench:
	go test -bench . -benchmem


.PHONY: \
	spell \
	lint \
	test \
	bench \
