/*
Package deque provides a flexible implementation of the deque ADT.
*/
package deque

// Deque defines the deque ADT.
type Deque interface {
	// Returns the current length of the deque.
	Length() int

	// Returns the current capacity of the deque. Returns -1 for inf capacity.
	Capacity() int

	// Returns the element at the head of the deque; does NOT remove it. Panics
	// if the deque is empty.
	Front() interface{}

	// Returns the element at the back of the deque; does NOT remove it. Panics
	// if the deque is empty.
	Back() interface{}

	// Adds an element to the head of the deque.
	PushFront(elem interface{})

	// Adds an element tot he tail of the deque.
	PushBack(elem interface{})

	// Removes the element at the head of the deque. Panics if the deque is empty.
	PopFront()

	// Removes the element at the tail of the deque. Panics if the deque is empty.
	PopBack()
}

type options struct {
	backend Deque
	safe    bool
}

// Option configures aspects of the deque.
type Option func(*options)

// Safe returns an Option that synchronizes access to the underlying deque
// implementation.
func Safe() Option {
	return func(o *options) {
		o.safe = true
	}
}

// Backend returns an Option that sets the deque backend.
func Backend(d Deque) Option {
	return func(o *options) {
		o.backend = d
	}
}

// New creates a new deque.
func New(opt ...Option) Deque {
	var opts options
	for _, o := range opt {
		o(&opts)
	}

	// Sets the default backend if unspecified.
	if opts.backend == nil {
		opts.backend = NewLD()
	}

	if opts.safe {
		return safe(opts.backend)
	}
	return opts.backend
}
