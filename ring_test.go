package deque

import (
	"testing"
	"time"
)

func TestRingBasic(t *testing.T) {
	nActions := 997

	deque := NewRD()
	err := testFront(deque, nActions)
	if err != nil {
		t.Fatalf("Failed frontFunc(): %v", err)
	}

	err = testBack(deque, nActions)
	if err != nil {
		t.Fatalf("Failed backFunc(): %v", err)
	}

	err = testMixed(deque, 1, nActions)
	if err != nil {
		t.Fatalf("Failed fixed mixedFunc(): %v", err)
	}

	seed := time.Now().UTC().UnixNano()
	err = testMixed(deque, seed, nActions)
	if err != nil {
		t.Fatalf("Failed random mixedFunc(): %v", err)
	}
}

// Tests the ring-buffer implementation for at capacity behaviour.
func TestRingCapacity(t *testing.T) {
	deque := NewRD()

	capacity := deque.Capacity()
	// Add entries to cause a growth in deque capacity.
	for i := 0; i < capacity+1; i++ {
		deque.PushBack(i)
	}

	if deque.Capacity() != capacity*2 {
		t.Fatalf("The deque capacity failed to double in size: %v", deque)
	}

	// Remove entries to trigger a shrink in deque capacity.
	for deque.Length() > capacity/2 {
		deque.PopBack()
	}

	if deque.Capacity() != capacity {
		t.Fatalf("The deque capacity failed to shrink in size: %v", deque)
	}
}

func BenchmarkRing(b *testing.B) {
	deque := NewRD()
	b.ResetTimer()
	benchmarkMixed(deque, 0, b.N)
}

func BenchmarkRingSafe(b *testing.B) {
	deque := safe(NewRD())
	b.ResetTimer()
	benchmarkMixed(deque, 0, b.N)
}
