package deque

import (
	"fmt"
	"math/rand"
)

// A simple test checking for consistency when operating with the front of the
// deque.
func testFront(deq Deque, n int) error {
	// Add elements to the front of the deque and check consistency.
	for elem := 0; elem < n; elem++ {
		deq.PushFront(elem)
		if deq.Front() != elem {
			return fmt.Errorf("PushFront() gave %v want %v\n%v", deq.Back(), elem, deq)
		}
	}

	// In the reverse direction, check and remove.
	for elem := n - 1; elem >= 0; elem-- {
		if deq.Front() != elem {
			return fmt.Errorf("PopFront() gave %v want %v\n%v", deq.Back(), elem, deq)
		}
		deq.PopFront()
	}
	return nil
}

// A simple test checking for consistency when operating with the back of the
// deque.
func testBack(deq Deque, n int) error {
	// Add elements to the back of the deque and check consistency.
	for elem := 0; elem < n; elem++ {
		deq.PushBack(elem)
		if deq.Back() != elem {
			return fmt.Errorf("PushBack() gave %v want %v\n%v", deq.Back(), elem, deq)
		}
	}

	// In the reverse direction, check and remove.
	for elem := n - 1; elem >= 0; elem-- {
		if deq.Back() != elem {
			return fmt.Errorf("PopBack() gave %v want %v\n%v", deq.Back(), elem, deq)
		}
		deq.PopBack()
	}
	return nil
}

// A mixed add to (front|back) remove from (front|back) test. Though it
// exposes a seed, this is inherently a random test, and is thrown in as a
// simple wrench to see if anything goes awry.
func testMixed(deq Deque, seed int64, n int) error {
	r := rand.New(rand.NewSource(seed))
	// Stores the location (0 for back, 1 for front) for each value as
	// represented by the index in the array.
	loc := make([]int, n)

	for elem := 0; elem < n; elem++ {
		if r.Int()%2 == 0 {
			// Add to the back of the deque.
			loc[elem] = 0
			deq.PushBack(elem)
			if deq.Back() != elem {
				return fmt.Errorf("PushBack() gave %v want %v\n%v", deq.Back(), elem, deq)
			}
		} else {
			// Add to the front of the deque.
			loc[elem] = 1
			deq.PushFront(elem)
			if deq.Front() != elem {
				return fmt.Errorf("PushFront() gave %v want %v\n%v", deq.Back(), elem, deq)
			}
		}
	}

	for elem := n - 1; elem >= 0; elem-- {
		if loc[elem] == 0 {
			if deq.Back() != elem {
				return fmt.Errorf("PopBack() gave %v want %v\n%v", deq.Back(), elem, deq)
			}
			deq.PopBack()
		} else {
			if deq.Front() != elem {
				return fmt.Errorf("PopFront() gave %v want %v\n%v", deq.Back(), elem, deq)
			}
			deq.PopFront()
		}
	}
	return nil
}

// A simple benchmarking function that performs all the possible mutating
// actions on a deque.
func benchmarkMixed(deque Deque, seed int64, iters int) {
	r := rand.New(rand.NewSource(seed))
	for i := 0; i < iters; i++ {
		// Randomly select one of the four operations to perform.
		switch r.Int()%2 + r.Int()%2 {
		case 0:
			deque.PushBack(i)
		case 1:
			deque.PushFront(i)
		case 2:
			if deque.Length() <= 0 {
				deque.PushBack(i)
				continue
			}
			deque.PopBack()
		case 3:
			if deque.Length() <= 0 {
				deque.PushFront(i)
				continue
			}
			deque.PopFront()
		}
	}
}
