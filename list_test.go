package deque

import (
	"testing"
	"time"
)

func TestListBasic(t *testing.T) {
	nActions := 997

	deque := NewLD()
	err := testFront(deque, nActions)
	if err != nil {
		t.Fatalf("Failed frontFunc(): %v", err)
	}

	err = testBack(deque, nActions)
	if err != nil {
		t.Fatalf("Failed backFunc(): %v", err)
	}

	err = testMixed(deque, 1, nActions)
	if err != nil {
		t.Fatalf("Failed fixed mixedFunc(): %v", err)
	}

	seed := time.Now().UTC().UnixNano()
	err = testMixed(deque, seed, nActions)
	if err != nil {
		t.Fatalf("Failed random mixedFunc(): %v", err)
	}
}

func BenchmarkList(b *testing.B) {
	deque := NewLD()
	b.ResetTimer()
	benchmarkMixed(deque, 0, b.N)
}

func BenchmarkListSafe(b *testing.B) {
	deque := safe(NewLD())
	b.ResetTimer()
	benchmarkMixed(deque, 0, b.N)
}
