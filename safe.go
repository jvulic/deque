package deque

import "sync"

/*
	A simple wrapper on any Deque that synchronizes access by means applying a
	RWMutex to all reads and writes.
*/

type safeDeque struct {
	sync.RWMutex
	in Deque
}

func safe(d Deque) Deque {
	return &safeDeque{in: d}
}

func (d *safeDeque) Length() int {
	d.RLock()
	defer d.RUnlock()
	return d.in.Length()
}

func (d *safeDeque) Capacity() int {
	d.RLock()
	defer d.RUnlock()
	return d.in.Capacity()
}

func (d *safeDeque) Front() interface{} {
	d.RLock()
	defer d.RUnlock()
	return d.in.Front()
}

func (d *safeDeque) Back() interface{} {
	d.RLock()
	defer d.RUnlock()
	return d.in.Back()
}

func (d *safeDeque) PushFront(elem interface{}) {
	d.Lock()
	defer d.Unlock()
	d.in.PushFront(elem)
}

func (d *safeDeque) PushBack(elem interface{}) {
	d.Lock()
	defer d.Unlock()
	d.in.PushBack(elem)
}

func (d *safeDeque) PopFront() {
	d.Lock()
	defer d.Unlock()
	d.in.PopFront()
}

func (d *safeDeque) PopBack() {
	d.Lock()
	defer d.Unlock()
	d.in.PopBack()
}
