package deque

/*
	A deque implemenation using a slice and slice operations.
*/

const gMinSliceDequeCapacity = 8

type sliceDeque struct {
	b []interface{}
}

// NewSD creates a new slice based deque.
func NewSD() Deque {
	return &sliceDeque{b: make([]interface{}, gMinSliceDequeCapacity)}
}

func (d *sliceDeque) Length() int {
	return len(d.b)
}

func (d *sliceDeque) Capacity() int {
	return cap(d.b)
}

func (d *sliceDeque) Front() interface{} {
	if len(d.b) <= 0 {
		panic("deque: Front() called on an empty deque")
	}
	return d.b[0]
}

func (d *sliceDeque) Back() interface{} {
	if len(d.b) <= 0 {
		panic("deque: Back() called on an empty deque")
	}
	return d.b[len(d.b)-1]
}

func (d *sliceDeque) PushFront(elem interface{}) {
	d.b = append([]interface{}{elem}, d.b...)
}

func (d *sliceDeque) PushBack(elem interface{}) {
	d.b = append(d.b, elem)
}

func (d *sliceDeque) PopFront() {
	if len(d.b) <= 0 {
		panic("deque: PopFront() called on an empty deque")
	}
	d.b = d.b[1:]
}

func (d *sliceDeque) PopBack() {
	if len(d.b) <= 0 {
		panic("deque: PopBack() called on an empty deque")
	}
	d.b = d.b[:len(d.b)-1]
}
