package deque

/*
	A deque implemenation using a ring buffer.

	Due to the clever use of the internal buffer this implementation provides
	memory, and usually some time benefits. Also fewer GC pauses.
*/

const gMinRingDequeCapacity = 8

type ringDeque struct {
	buf               []interface{}
	head, tail, count int
}

// NewRD creates a new ring-buffer based deque.
func NewRD() Deque {
	return &ringDeque{
		buf: make([]interface{}, gMinRingDequeCapacity),
	}
}

// Resizes the deque to fit exactly twice the deque's current contents.
func (d *ringDeque) resize() {
	newBuf := make([]interface{}, d.count*2)

	if d.tail > d.head {
		// Straight-forward copy into the new buffer.
		copy(newBuf, d.buf[d.head:d.tail])
	} else {
		// The tail has wrapped around the internal buffer so we need to perform
		// two copies in order to copy the data and preserve order.
		n := copy(newBuf, d.buf[d.head:])
		copy(newBuf[n:], d.buf[:d.tail])
	}
	d.head = 0
	d.tail = d.count
	d.buf = newBuf
}

func (d *ringDeque) Length() int {
	return d.count
}

func (d *ringDeque) Capacity() int {
	return cap(d.buf)
}

func (d *ringDeque) Front() interface{} {
	if d.count <= 0 {
		panic("deque: Front() called on an empty deque")
	}
	return d.buf[d.head]
}

func (d *ringDeque) Back() interface{} {
	if d.count <= 0 {
		panic("deque: Back() called on an empty deque")
	}
	idx := (d.tail + cap(d.buf) - 1) % cap(d.buf)
	return d.buf[idx]
}

func (d *ringDeque) PushFront(elem interface{}) {
	if d.count == cap(d.buf) {
		d.resize()
	}
	// Go carries the sign of the dividend to the result of the modulo. We
	// normalize the result to avoid negative indexes.
	d.head = (d.head + cap(d.buf) - 1) % cap(d.buf)
	d.buf[d.head] = elem
	d.count++
}

func (d *ringDeque) PushBack(elem interface{}) {
	if d.count == cap(d.buf) {
		d.resize()
	}
	d.buf[d.tail] = elem
	d.tail = (d.tail + 1) % cap(d.buf)
	d.count++
}

func (d *ringDeque) PopFront() {
	if d.count <= 0 {
		panic("deque: PopFront() called on an empty deque")
	}
	d.buf[d.head] = nil
	d.head = (d.head + 1) % cap(d.buf)
	d.count--

	// If greater than the minimum capacity and only using 25% of the total
	// current capacity, shrink the buffer.
	if cap(d.buf) > gMinRingDequeCapacity && d.count*4 == cap(d.buf) {
		d.resize()
	}
}

func (d *ringDeque) PopBack() {
	if d.count <= 0 {
		panic("deque: PopBack() called on an empty deque")
	}
	// Go carries the sign of the dividend to the result of the modulo. We
	// normalize the result to avoid negative indexes.
	d.tail = (d.tail + cap(d.buf) - 1) % cap(d.buf)
	d.buf[d.tail] = nil
	d.count--

	// If greater than the base capacity, shrink the deque if at 25% the capacity.
	if cap(d.buf) > gMinRingDequeCapacity && d.count*4 == cap(d.buf) {
		d.resize()
	}
}
