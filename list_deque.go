package deque

import (
	"container/list"
)

/*
	Implements a deque using a doubly-linked list.
*/

type listDeque struct {
	l *list.List
}

// NewLD creates a new doubly-linked list based deque.
func NewLD() Deque {
	return &listDeque{l: list.New()}
}

func (d *listDeque) Length() int {
	return d.l.Len()
}

func (d *listDeque) Capacity() int {
	return -1
}

func (d *listDeque) Front() interface{} {
	if d.l.Len() <= 0 {
		panic("deque: Front() called on an empty deque")
	}
	return d.l.Front().Value
}

func (d *listDeque) Back() interface{} {
	if d.l.Len() <= 0 {
		panic("deque: Back() called on an empty deque")
	}
	return d.l.Back().Value
}

func (d *listDeque) PushFront(elem interface{}) {
	d.l.PushFront(elem)
}

func (d *listDeque) PushBack(elem interface{}) {
	d.l.PushBack(elem)
}

func (d *listDeque) PopFront() {
	if d.l.Len() <= 0 {
		panic("deque: PopFront() called on an empty deque")
	}
	d.l.Remove(d.l.Front())
}

func (d *listDeque) PopBack() {
	if d.l.Len() <= 0 {
		panic("deque: PopBack() called on an empty deque")
	}
	d.l.Remove(d.l.Back())
}
